﻿<!DOCTYPE html>
<html>
<head>

	<?php
		include "../../elements/head.php"
	?>
	
                    <script src="../../scripts/jquery.js"></script>
                    <script src="../../scripts/d3.js"></script>
                    <script src="../../scripts/nv.d3.min.js"></script>
                    <script src="../../scripts/angular.min.js"></script>
                    <script src="../../scripts/angular-nvd3.min.js"></script>
</head>
<body>
	<header>
		<?php
			include "../../elements/header.php"
		?>
	</header>
	<main>
	
		<article class="container">
        <div class="row">
            <div class="col-lg-12">
           
			

                   

				   
                    <div class="chartbox">
                        <div >
                            <h1 class="page-header">
                                Basic Bar Chart
                                <br /> <small>D3 using SVG and Javascript</small>
                            </h1>
                            <div id="chart"></div>
                            <script src="../../js/basicbarchart-d3.js"></script>

                            
                        </div>
                    </div>
                
        </div>
     
            </div>

	</main>

	<footer>	
		<?php
			include "../../elements/footer.php"
		?>
	</footer>
</body>
</html>
