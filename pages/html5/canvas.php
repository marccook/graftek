﻿<!DOCTYPE html>
<html>
<head>

	<?php
		include "../../elements/head.php"
	?>
	
</head>
<body>
	<header>
		<?php
			include "../../elements/header.php"
		?>
	</header>
	<main>
	
		<article class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>HTML Canvas site Under Construction! <small> - This is a project that I've recently started</small></h1>
				<script src="../../js/basicbarchart-canvas.js"></script>
				<script src="../../scripts/canvasjs.min.js"></script>
                
            </div>
        </div>

	</main>

	<footer>	
		<?php
			include "../../elements/footer.php"
		?>
	</footer>
</body>
</html>
