﻿<!DOCTYPE html>
<html>
<head>

	<?php
		include "../../elements/head.php"
	?>
				<script src="../../scripts/canvasjs.min.js"></script>
	
</head>
<body>
	<header>
		<?php
			include "../../elements/header.php"
		?>
	</header>
	<main>
	</br></br>
		<article class="container">
        <div class="row">
        
		</article>
				<article>
					<script src="../../js/basicbarchart-canvas.js"></script>
					<div id="BarChartContainer" style="height: 400px; width: 100%;"></div>
				</article></div>
	</main>

	<footer>	
		<?php
			include "../../elements/footer.php"
		?>
	</footer>
</body>
</html>
