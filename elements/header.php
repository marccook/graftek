﻿
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- You'll want to use a responsive image option so this logo looks good on devices - I recommend using something like retina.js (do a quick Google search for it and you'll find it) -->
				
        <hgroup class="col-sm-12 col-lg-12">
          <a href="../../index.php"><h1><strong>GrafTek</strong> <small> - Because your data has a story to tell</small></h1></a>
        </hgroup>
                
            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">                    
                    <li id="portfolio" class="dropdown">
                        <a href="../../pages/d3svg/d3js.php" class="dropdown-toggle" data-toggle="dropdown">D3.js & SVG<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li id="1columnportfolio">
                              <li>
                                <a href="../../pages/d3svg/d3js.php"></a>
                              </li>
                              <li>
                                <a href="../../pages/d3svg/svg.php"></a>
                              </li>
                              
                            </li>
                        </ul>
                    </li>
                  

                    <li id="other" class="dropdown">
                        <a href="../../pages/html5/html5.php" class="dropdown-toggle" data-toggle="dropdown">Raw HTML5 Elements<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li id="1columnportfolio">
                              <a href="../../pages/html5/html5.php"></a>
                              
                            </li>
                        </ul>
                    </li>


                    <li id="other" class="dropdown">
						        <a href="../../pages/highchart/highchart.php" class="dropdown-toggle" data-toggle="dropdown">Highcharts<b class="caret"></b></a>
						          <ul class="dropdown-menu">
							          <li id="fullwidthpage">
                          <a href="../../pages/highchart/highchart.php"></a>                
					              </li>
                      </ul>
                        </li>
                        <li id="other" class="dropdown">
                        <a href="../../pages/jqplot/jqplot.php" class="dropdown-toggle" data-toggle="dropdown">jqPlot<b class="caret"></b></a>
                      <ul class="dropdown-menu">
                            <li id="fullwidthpage">
                              <a href="../../pages/jqplot/jqplot.php"></a>
                              
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>