﻿angular.module("PizzaApp", ["increment", "ngResource"])
.constant("baseUrl", "http://localhost:49002/")
.controller("pizzaCtrl", function ($scope, $http, $resource, baseUrl) {

    $scope.displayMode = "list";
    $scope.currentProduct = null;

    $scope.pizzaResource = $resource(baseUrl + ":id", { id: "@id" },
            { create: { method: "POST" }, save: { method: "PUT" } });

    $scope.listPizzas = function () {
        $scope.pizzas = $scope.pizzasResource.query();
    }

    $scope.deletePizza = function (pizza) {
        pizza.$delete().then(function () {
            $scope.pizzas.splice($scope.pizza.indexOf(pizza), 1);
        });
        $scope.displayMode = "list";
    }

    $scope.createPizza = function (pizza) {
        new $scope.pizzasResource(pizza).$create().then(function (newPizza) {
            $scope.pizzas.push(newPizza);
            $scope.displayMode = "list";
        });
    }


    $scope.updatePizza = function (pizza) {
        pizza.$save();
        $scope.displayMode = "list";
    }

    $scope.editOrCreatePizza = function (pizza) {
        $scope.currentProduct = pizza ? pizza : {};
        $scope.displayMode = "edit";
    }

    $scope.saveEdit = function (pizza) {
        if (angular.isDefined(pizza.id)) {
            $scope.updatePizza(pizza);
        } else {
            $scope.createPizza(pizza);
        }
    }

    $scope.cancelEdit = function () {
        if ($scope.currentPizza && $scope.createPizza.$get) {
            $scope.currentPizza.$get();
        }
        $scope.currentPizza = {};
        $scope.displayMode = "list";
    }

    $scope.lisPizza();
});
