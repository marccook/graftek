angular.module("OrderApp", ["increment", "ngResource"])
.constant("baseUrl", "http://localhost:49002/")
.controller("orderCtrl", function ($scope, $http, $resource, baseUrl) {

    $scope.displayMode = "list";
    $scope.currentProduct = null;

    $scope.orderResource = $resource(baseUrl + ":id", { id: "@id" },
            { create: { method: "POST" }, save: { method: "PUT" } });

    $scope.listOrders = function () {
        $scope.orders = $scope.ordersResource.query();
    }

    $scope.deleteOrder = function (order) {
        order.$delete().then(function () {
            $scope.orders.splice($scope.order.indexOf(order), 1);
        });
        $scope.displayMode = "list";
    }

    $scope.createOrder = function (order) {
        new $scope.ordersResource(order).$create().then(function (neworder) {
            $scope.orders.push(newOrder);
            $scope.displayMode = "list";
        });
    }


    $scope.updateOrder = function (order) {
        order.$save();
        $scope.displayMode = "list";
    }

    $scope.editOrCreateOrder = function (order) {
        $scope.currentProduct = order ? order : {};
        $scope.displayMode = "edit";
    }

    $scope.saveEdit = function (order) {
        if (angular.isDefined(order.id)) {
            $scope.updateOrder(order);
        } else {
            $scope.createOrder(order);
        }
    }

    $scope.cancelEdit = function () {
        if ($scope.currentOrder && $scope.createOrder.$get) {
            $scope.currentOrder.$get();
        }
        $scope.currentOrder = {};
        $scope.displayMode = "list";
    }

    $scope.listOrder();
});
