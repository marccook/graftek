﻿angular.module("CustomerApp", ["increment", "ngResource"])
.constant("baseUrl", "http://localhost:49002/")
.controller("customerCtrl", function ($scope, $http, $resource, baseUrl) {

    $scope.displayMode = "list";
    $scope.currentProduct = null;

    $scope.customerResource = $resource(baseUrl + ":id", { id: "@id" },
            { create: { method: "POST" }, save: { method: "PUT" } });

    $scope.listCustomers = function () {
        $scope.customer = $scope.customersResource.query();
    }

    $scope.deleteCustomer = function (customer) {
        customer.$delete().then(function () {
            $scope.customers.splice($scope.customer.indexOf(customer), 1);
        });
        $scope.displayMode = "list";
    }

    $scope.createCustomer = function (customer) {
        new $scope.customersResource(customer).$create().then(function (newcustomer) {
            $scope.customers.push(newCustomer);
            $scope.displayMode = "list";
        });
    }


    $scope.updateCustomer = function (customer) {
        customer.$save();
        $scope.displayMode = "list";
    }

    $scope.editOrCreateCustomer = function (customer) {
        $scope.currentProduct = customer ? customer : {};
        $scope.displayMode = "edit";
    }

    $scope.saveEdit = function (customer) {
        if (angular.isDefined(customer.id)) {
            $scope.updateCustomer(customer);
        } else {
            $scope.createCustomer(customer);
        }
    }

    $scope.cancelEdit = function () {
        if ($scope.currentCustomer && $scope.createCustomer.$get) {
            $scope.currentCustomer.$get();
        }
        $scope.currentCustomer = {};
        $scope.displayMode = "list";
    }

    $scope.listCustomer();
});
