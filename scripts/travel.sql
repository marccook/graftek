use master;
go

drop database TravelDB;
go

create database TravelDB;
go

use TravelDB;
go

create schema Travel;
go


--Create Tables for the Database
Create table Travel.Employee
(
  EmployeeID int not null identity(1,1),
  CompanyID			int		       not null,
  Department	    varchar(30)	   not null,
  Prefix            varchar(10)        null, 
  FirstName         varchar(50)    not null, 
  MiddeName         varchar(50)        null,
  LastName          varchar(50)    not null,
  Suffix            varchar(20)        null, 
  Address1          varchar(250)       null, 
  Address2          varchar(250)       null, 
  City              varchar(50)        null, 
  [State]           varchar(50)        null, 
  ZipCode           varchar(12)        null, 
  DateModified      datetime2(0)       null
); 
go


create table Travel.Company
(
  CompanyID int not null identity(1,1),
  CompanyName        varchar(50)  not null,
  CompanyDesc        varchar(50)   not null,
  CompanyNotes       varchar(250)     null,
  DateModified       datetime2(0)     null
); 
go


create table Travel.Package 
(
  PackageID int not null identity(1,1),
  TransportationID  int                null,
  LodgingID		    int                null,
  [Description]		varchar(250)       null,
  Price		        Numeric(5,2)       null,
  DateModified    datetime2(0)         null
);
go



create table Travel.[Order]
(
  OrderID int not null identity(1,1),
  EmployeeID    int				not null, 
  PackageID     int             not null,
  TotalPrice    numeric(5,2)        null,  
  Notes		    varchar(50)         null,
  Orderdate     datetime2(0)    not null, 
  DateModified  datetime2(0)        null,
); --Create 
go


create table Travel.Lodging
(
  LodgingID         int not null identity(1,1),
  LodgingName	    varchar(50)     not null,
  [Location] 	    varchar(50)     not null,
  StartDate         datetime2(0)   not null,
  EndDate           datetime2(0)   not null,
  DateModified      datetime2(0)    null,
);
go


create table Travel.Transportation
(
  TransportationID        int not null identity(1,1),
  FlightID     int             null,
  TrainID      int             null,
  BusID        int             null,
  DateModified      datetime2(0)    null,
);
go


create table Travel.Flight
(
  FlightID int not null identity(1,1),
  Airline	    varchar   (50)  not null, 
  Origin        varchar   (3)   not null,  
  Destination   varchar   (50)  not null,
  Departure     datetime2(0)    not null, 
  Arival        datetime2(0)    not null, 
  Price         numeric(5,2)    not null, 
  DateModified  datetime2(0)        null,
); 
go


create table Travel.Train
(
  TrainID int not null identity(1,1),
  Trainline	    varchar   (50)  not null, 
  Origin        varchar   (3)   not null,  
  Destination   varchar   (50)  not null,
  Departure     datetime2(0)    not null, 
  Arival        datetime2(0)    not null, 
  DateModified  datetime2(0)        null,
);
go



create table Travel.Bus
(
  BusID int not null identity(1,1),
  Busline	    varchar   (50)  not null, 
  Origin        varchar   (3)   not null,  
  Destination   varchar   (50)  not null,
  Departure     datetime2(0)    not null, 
  Arival        datetime2(0)    not null, 
  DateModified  datetime2(0)        null,
);
go



--------------------------------------------------------------------------------------
--    Add Derived fileds to the tables
--------------------------------------------------------------------------------------
--ALTER TABLE (Register.Course, Register.Department) ADD CourseCode AS (Department.DepartmentCode + Course.CourseLevel);
--go

--Select *
--From Register.Department;


--------------------------------------------------------------------------------------
--    create Primar keys for the tables
--------------------------------------------------------------------------------------
ALTER TABLE Travel.Employee
  ADD constraint PK_Travel_EmployeeID Primary Key clustered (EmployeeID);
  go

ALTER TABLE Travel.Package
  ADD constraint PK_Travel_PackageID Primary Key clustered (PackageID);
  go

ALTER TABLE Travel.Company
  ADD constraint PK_Travel_CompanyID Primary Key clustered (CompanyID);
  go

ALTER TABLE Travel.[Order]
  ADD constraint PK_Travel_OrderID Primary Key clustered (OrderID);
  go

ALTER TABLE Travel.Transportation
  ADD constraint PK_Travel_TransportationID Primary Key clustered (TransportationID);
  go

ALTER TABLE Travel.Lodging
  ADD constraint PK_Travel_LodgingID Primary Key clustered (LodgingID);
  go

ALTER TABLE Travel.Flight
  ADD constraint PK_Travel_FlightID Primary Key clustered (FlightID);
  go
  
ALTER TABLE Travel.Train
  ADD constraint PK_Travel_TrainID Primary Key clustered (TrainID);
  go


ALTER TABLE Travel.Bus
  ADD constraint PK_Travel_BusID Primary Key clustered (BusID);
  go




------------------------------------------------------------------------------------------------
--        alter tables - foreign keys.
-------------------------------------------------------------------------------------------------

-------Employee Table
alter table Travel.Employee
  add constraint FK_Employee_Company foreign key (CompanyID) references Travel.Company (CompanyID);
go


-------Order Table
alter table Travel.[Order]
  add constraint FK_Order_Company foreign key (CompanyID) references Travel.Company (CompanyId);
go

alter table Travel.[Order]
  add constraint FK_Order_Package foreign key (PackageID) references Travel.Package (PackageId);
go


-------Package Table
alter table Travel.Package
  add constraint FK_Package_Transportation foreign key (TransportationID) references Travel.Transportation (TransportationId);
go

alter table Travel.Package
  add constraint FK_Package_Lodging foreign key (LodgingID) references Travel.Lodging (LodgingId);
go

-------Transportation Table
alter table Travel.Transportation
  add constraint FK_Transportation_Flight foreign key (FlightID) references Travel.Flight (FlightId);
go


alter table Travel.Transportation
  add constraint FK_Transportation_Train foreign key (TrainID) references Travel.Train (TrainId);
go

-------Registration Table
alter table Travel.Transportation
  add constraint FK_Transportation_Bus foreign key (BusID) references Travel.Bus (BusId);
go


-----------------------------------------------
--set the check constraints.
-----------------------------------------------

---Student Prefix
alter table Travel.Employee
  add constraint ck_Employee_Prefix 
  check (Prefix in 
  (
    '', 'Mr.', 'Dr.', 'Mrs.', 'Ms.', 'Miss', 'Mrs.'
  ));
go

---Student Suffix
alter table Travel.Employee
  add constraint ck_Employee_Suffix 
  check (Suffix in 
  (
    '', 'Jr.', 'Sr.', 'I', 'II', 'III', 'IV'
  ));
go



----------------------------------------------------------------------------
--         Create Views
----------------------------------------------------------------------------
--Drop view Register.Schedule;
--go

--Create view Register.Schedule
--AS
--Select rs.FirstName as 'First Name', rs.LastName as 'Last Name',  rc.CourseDays as 'Days', rc.StartTime as 'Start Time', rc.EndTime as 'End Time'
--From Register.Registration as rr, Register.Student as rs, Register.Course as rc;
--go


create procedure EmployeeProcedure
(@Employee varchar)
as
begin
select  TE.EmployeeID, TE.CompanyID, TE.Department, TE.Prefix, TE.FirstName, TE.MiddeName, TE.LastName, TE.Suffix, TE.Address1, TE.Address2, TE.City, TE.[State], TE.ZipCode, TE.DateModified
from Travel.Employee TE;
END
go

create procedure BusProcedure
(@Bus varchar)
as
begin
select TB.BusID, TB.Busline, TB.Origin, TB.Departure, TB.Arival, TB.DateModified
from Travel.Bus TB;
END
go


create procedure CompanyProcedure
(@Company varchar)
as
begin
select TC.CompanyID, TC.CompanyName, TC.CompanyDesc, TC.CompanyNotes, TC.DateModified
from Travel.Company TC;
END
go

create procedure FlightProcedure
(@Flight varchar)
as
begin
select TF.FlightID, TF.Airline, TF.Origin, TF.Destination, TF.Departure, TF.Arival, TF.Price, TF.DateModified
from Travel.Flight TF;
END
go

create procedure LodgingProcedure
(@Lodging varchar)
as
begin
select TL.LodgingID, TL.LodgingName, TL.[Location], TL.StartDate, TL.EndDate, TL.DateModified
from Travel.Lodging TL;
END
go


create procedure OrderProcedure
(@Order varchar)
as
begin
select [TO].OrderID, [TO].CompanyID, [TO].PackageID, [TO].TotalPrice, [TO].Notes, [TO].Orderdate, [TO].DateModified
from Travel.[Order] [TO];
END
go

create procedure PackageProcedure
(@Package varchar)
as
begin
select TP.PackageID, TP.TransportationID, TP.LodgingID, TP.[Description], TP.Price, TP.DateModified
from Travel.Package TP;
END
go

create procedure TrainProcedure
(@Train varchar)
as
begin
select TT.TrainID, TT.Trainline, TT.Origin, TT.Destination, TT.Departure, TT.Arival, TT.DateModified
from Travel.Train TT;
END
go

create procedure TransportationProcedure
(@Transprtation varchar)
as
begin
select TTr.TransportationID, TTr.FlightID, TTr.TrainID, TTr.BusID
from Travel.Transportation TTr;
END
go






--Select * 
--from Travel.package, Travel.[Order], Travel.Company
--WHERE Package.PackageID = [Order].PackageID;
--go

create procedure EmployeeByStateProcedure
(@Empbystate varchar)
as
begin
select  Concat(Employee.LastName, Employee.FirstName) AS 'name', Employee.[State]
from Travel.Employee;
END
go

create procedure EmployeeBy2016TotalProcedure
(@EmployeeByYearTotal varchar)
as
begin
select  Concat(Employee.LastName, Employee.FirstName) AS name , [Order].TotalPrice as 'TotalPrice'
from Travel.Employee, Travel.Company, Travel.[Order]
INNER JOIN Travel.Employee TE on TE.CompanyID = [Order].CompanyID
inner join Travel.Company TC on TC.CompanyID = [Order].CompanyID

Order by name;
END
go


create procedure CompanyTotalProcedure
(@EmployeeBy2016Total varchar)
as
begin
select  TC.CompanyID AS 'Company Name', [Order].TotalPrice as 'Total Spent by Company'
from Travel.Employee, Travel.[Order]
inner join Travel.Company TC on TC.CompanyID = [Order].CompanyID
END
go



create procedure SP_Contact_FindPersonCount @name nvarchar(50)
as
begin
  declare @firstCount int;
  declare @lastCount int;
  declare @count int;

  select @firstCount = count(*)
  from Travel.Employee
  where @name = FirstName;

  select @lastCount = count(*)
  from Travel.Employee
  where @name = LastName;

  set @count = @firstCount + @lastCount;
  print @count;
end;
go

exec EmployeeByStateProcedure;
go



insert into Travel.Company (Company.CompanyName, Company.CompanyDesc, Company.CompanyNotes)
 values 
 ('1234', '987', 'This company is cool');
 go


insert into Travel.Employee (Employee.CompanyID, Employee.Department, Employee.Prefix, Employee.FirstName, Employee.MiddeName, Employee.LastName, Employee.Suffix, Employee.Address1, Employee.Address2, Employee.City, Employee.[State], Employee.ZipCode)
 values 
 ('1', 'Marketting', 'Mr.', 'Guy', 'Micheal', 'Smiley', 'III', '121212 Diver Dr', 'Apt 99999', 'St. Luis', 'MO', '64999');
go

select * 
From Travel.Employee;
go

