﻿<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>

  <title>GrafTek's Website Home Page</title>


   <!--Bootstrap Core CSS--> 
  <link href="css/bootstrap.min.css" rel="stylesheet"/>

   <!--Custom CSS--> 
  <link href="css/modern-business.css" rel="stylesheet"/>

   <!--Custom Fonts--> 
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="css/jquery.carousel-3d.default.css" />

  <script src="scripts/jquery.js"></script>
  <script src="scripts/jquery.resize.js"></script>
  <script src="scripts/jquery.waitforimages.js"></script>
  <script src="scripts/modernizr.js"></script>
  <script src="scripts/jquery.carousel-3d.js" ></script>
</head>
<body>
	<header>
		

		
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- You'll want to use a responsive image option so this logo looks good on devices - I recommend using something like retina.js (do a quick Google search for it and you'll find it) -->
				
        <hgroup class="col-sm-12 col-lg-12">
          <a href="index.php"><h1><strong>GrafTek</strong> <small> - Because your data has a story to tell</small></h1></a>
        </hgroup>
                
            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">                    
                    <li id="portfolio" class="dropdown">
                        <a href="pages/d3svg/d3js.php" class="dropdown-toggle" data-toggle="dropdown">D3.js & SVG<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li id="1columnportfolio">
                              <li>
                                <a href="pages/d3svg/d3js.php"></a>
                              </li>
                              <li>
                                <a href="pages/d3svg/svg.php"></a>
                              </li>
                              
                            </li>
                        </ul>
                    </li>
                  

                    <li id="other" class="dropdown">
                        <a href="pages/html5/html5.php" class="dropdown-toggle" data-toggle="dropdown">Raw HTML5 Elements<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li id="1columnportfolio">
                              <a href="pages/html5/html5.php"></a>                              
                            </li>
                            <li>
                              <a href="pages/html5/canvas.php"></a>                              
                            </li>
                        </ul>
                    </li>


                    <li id="other" class="dropdown">
						<a href="pages/highchart/highchart.php" class="dropdown-toggle" data-toggle="dropdown">Highchart<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li id="fullwidthpage"> 
								<a href="pages/highcharts/highchart.php"></a>               
							</li>
						</ul>
                   </li>

                   <li id="other" class="dropdown">
                        <a href="pages/jqplot/jqplot.php" class="dropdown-toggle" data-toggle="dropdown">jqPlot<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li id="fullwidthpage">
									<a href="pages/jqplot/jqplot.php"></a>                              
								</li>
							</ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>




	</header>
		<main>
		<?php
			include "elements/carousel.php"
		?>


		<article class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Website Under Construction! <small> - This is a project that I've recently started</small></h1>                
            </div>
        </div>
		</article>
<div class="section">

    <div class="container">

        <div class="row">
            <div class="col-lg-6 col-md-6">
                <h3><i class="fa fa-check-circle"></i>Visualizations</h3>
                <p>I'm using this Website as a testing ground for the differant graphing technologies that are available to create Vizualizations.</p>
            </div>
            <div class="col-lg-6 col-md-6">
                <h3><i class="fa fa-pencil"></i>Technologies</h3>
                <p>The main graphing technoologies I'm going to be using on this site are d3.js, sva, canvas, HTML5, CSS3, AngularJS Ajax, JSON, and XML. I'm sure more will be added here as I work on it.</p>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

</div>
<!-- /.section -->

	</main>

	<footer>	
		<?php
			include "elements/footer.php"
		?>
	</footer>
</body>
</html>
